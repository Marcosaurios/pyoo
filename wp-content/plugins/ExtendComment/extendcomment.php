<?php
/*
Plugin Name: Extend Comment
Version: 1.0
Plugin URI: http://smartwebworker.com
Description: A plug-in to add additional fields in the comment form.
Author: Specky Geek
Author URI: http://www.speckygeek.com
*/

// Add custom meta (ratings) fields to the default comment form
// Default comment form includes name, email and URL
// Default comment form elements are hidden when user is logged in

add_filter('comment_form_default_fields','custom_fields');
function custom_fields($fields) {

		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );

		$fields[ 'author' ] = '<p class="comment-form-author">'.
			'<label for="author">' . __( 'Name' ) . '</label>'.
			( $req ? '<span class="required">*</span>' : '' ).
			'<input id="author" name="author" type="text" value="'. esc_attr( $commenter['comment_author'] ) . 
			'" size="30" tabindex="1"' . $aria_req . ' /></p>';
		
		$fields[ 'email' ] = '<p class="comment-form-email">'.
			'<label for="email">' . __( 'Email' ) . '</label>'.
			( $req ? '<span class="required">*</span>' : '' ).
			'<input id="email" name="email" type="text" value="'. esc_attr( $commenter['comment_author_email'] ) . 
			'" size="30"  tabindex="2"' . $aria_req . ' /></p>';
					
		$fields[ 'url' ] = '<p class="comment-form-url">'.
			'<label for="url">' . __( 'Website' ) . '</label>'.
			'<input id="url" name="url" type="text" value="'. esc_attr( $commenter['comment_author_url'] ) . 
			'" size="30"  tabindex="3" /></p>';

		$fields[ 'phone' ] = '<p class="comment-form-phone">'.
			'<label for="phone">' . __( 'Phone' ) . '</label>'.
			'<input id="phone" name="phone" type="text" size="30"  tabindex="4" /></p>';

	return $fields;
}

// Add fields after default fields above the comment box, always visible

add_action( 'comment_form_logged_in_after', 'additional_fields' );
add_action( 'comment_form_after_fields', 'additional_fields' );

function additional_fields () {
	echo '<p class="comment-form-title">'.
	'<label for="title">' . __( 'Comment Title' ) . '</label>'.
	'<input id="title" name="title" type="text" size="30"  tabindex="5" /></p>';

	echo '<p class="comment-form-rating">'.
	'<label for="rating">'. __('Bateria') . '<span class="required">*</span></label>
	<span class="commentratingbox">';
	
	for( $i=1; $i <= 5; $i++ )
	echo '<span class="commentrating"><input type="radio" name="rating" id="rating" value="'. $i .'"/>'. $i .'</span>';

	echo'</span></p>';

  // asi anyade selector de pantalla debajo de Rating
  echo '<p class="comment-form-rating">'.
	'<label for="pantalla">'. __('Pantalla') . '<span class="required">*</span></label>
	<span class="commentratingbox">';
	
	for( $pa=1; $pa <= 5; $pa++ )
	echo '<span class="commentratingpa"><input type="radio" name="pantalla" id="pantalla" value="'. $pa .'"/>'. $pa .'</span>';

	echo'</span></p>';
  
  echo '<p class="comment-form-rating">'.
	'<label for="sonido">'. __('Sonido') . '<span class="required">*</span></label>
	<span class="commentratingbox">';
	
	for( $so=1; $so <= 5; $so++ )
	echo '<span class="commentratingso"><input type="radio" name="sonido" id="sonido" value="'. $so .'"/>'. $so .'</span>';

	echo'</span></p>';
}

// Save the comment meta data along with comment

add_action( 'comment_post', 'save_comment_meta_data' );
function save_comment_meta_data( $comment_id ) {
	if ( ( isset( $_POST['phone'] ) ) && ( $_POST['phone'] != '') )
	$phone = wp_filter_nohtml_kses($_POST['phone']);
	add_comment_meta( $comment_id, 'phone', $phone );

	if ( ( isset( $_POST['title'] ) ) && ( $_POST['title'] != '') )
	$title = wp_filter_nohtml_kses($_POST['title']);
	add_comment_meta( $comment_id, 'title', $title );

	if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != '') )
	$rating = wp_filter_nohtml_kses($_POST['rating']);
	add_comment_meta( $comment_id, 'rating', $rating );

  // con esto guarda en la BD pantalla y su variable
  if ( ( isset( $_POST['pantalla'] ) ) && ( $_POST['pantalla'] != '') )
	$pantalla = wp_filter_nohtml_kses($_POST['pantalla']);
	add_comment_meta( $comment_id, 'pantalla', $pantalla );

   if ( ( isset( $_POST['sonido'] ) ) && ( $_POST['sonido'] != '') )
	$sonido = wp_filter_nohtml_kses($_POST['sonido']);
	add_comment_meta( $comment_id, 'sonido', $sonido );



}


// Add the filter to check if the comment meta data has been filled or not

add_filter( 'preprocess_comment', 'verify_comment_meta_data' );
function verify_comment_meta_data( $commentdata ) {
	if ( ! isset( $_POST['pantalla'] ) )
	wp_die( __( 'Error: You did not add your rating. Hit the BACK button of your Web browser and resubmit your comment with rating.' ) );
	return $commentdata;
}

//Add an edit option in comment edit screen  

add_action( 'add_meta_boxes_comment', 'extend_comment_add_meta_box' );
function extend_comment_add_meta_box() {
    add_meta_box( 'title', __( 'Comment Metadata - Extend Comment' ), 'extend_comment_meta_box', 'comment', 'normal', 'high' );
}
 
function extend_comment_meta_box ( $comment ) {
    $phone = get_comment_meta( $comment->comment_ID, 'phone', true );
    $title = get_comment_meta( $comment->comment_ID, 'title', true );
    $rating = get_comment_meta( $comment->comment_ID, 'rating', true );
   	$pantalla = get_comment_meta( $comment->comment_ID, 'pantalla', true );
  	$sonido = get_comment_meta( $comment->comment_ID, 'sonido', true );  
  
    wp_nonce_field( 'extend_comment_update', 'extend_comment_update', false );
    ?>
    <p>
        <label for="phone"><?php _e( 'Phone' ); ?></label>
        <input type="text" name="phone" value="<?php echo esc_attr( $phone ); ?>" class="widefat" />
    </p>
    <p>
        <label for="title"><?php _e( 'Comment Title' ); ?></label>
        <input type="text" name="title" value="<?php echo esc_attr( $title ); ?>" class="widefat" />
    </p>
    <p>
        <label for="rating"><?php _e( 'Bateria: ' ); ?></label>
			<span class="commentratingbox">
			<?php for( $i=1; $i <= 5; $i++ ) {
				echo '<span class="commentrating"><input type="radio" name="rating" id="rating" value="'. $i .'"';
				if ( $rating == $i ) echo ' checked="checked"';
				echo ' />'. $i .' </span>'; 
				}
			?>
			</span>
    </p>
	
	<p> 
        <label for="pantalla"><?php _e( 'Pantalla: ' ); ?></label>
			<span class="commentratingboxpa">
			<?php for( $pa=1; $pa <= 5; $pa++ ) {
				echo '<span class="commentratingpa"><input type="radio" name="pantalla" id="pantalla" value="'. $pa .'"';
				if ( $pantalla == $pa ) echo ' checked="checked"';
				echo ' />'. $pa .' </span>'; 
				}
			?>
			</span>
    </p>

<p> 
        <label for="sonido"><?php _e( 'Sonido: ' ); ?></label>
			<span class="commentratingboxso">
			<?php for( $so=1; $so <= 5; $so++ ) {
				echo '<span class="commentratingpa"><input type="radio" name="sonido" id="sonido" value="'. $so .'"';
				if ( $sonido == $so ) echo ' checked="checked"';
				echo ' />'. $so .' </span>'; 
				}
			?>
			</span>
    </p>
    <?php
}

// modificando la funcion de arriba no he visto cambio en la BD. Debe ser algo que guarda en el comentario

// Update comment meta data from comment edit screen 

add_action( 'edit_comment', 'extend_comment_edit_metafields' );
function extend_comment_edit_metafields( $comment_id ) {
    if( ! isset( $_POST['extend_comment_update'] ) || ! wp_verify_nonce( $_POST['extend_comment_update'], 'extend_comment_update' ) ) return;

	if ( ( isset( $_POST['phone'] ) ) && ( $_POST['phone'] != '') ) : 
	$phone = wp_filter_nohtml_kses($_POST['phone']);
	update_comment_meta( $comment_id, 'phone', $phone );
	else :
	delete_comment_meta( $comment_id, 'phone');
	endif;
		
	if ( ( isset( $_POST['title'] ) ) && ( $_POST['title'] != '') ):
	$title = wp_filter_nohtml_kses($_POST['title']);
	update_comment_meta( $comment_id, 'title', $title );
	else :
	delete_comment_meta( $comment_id, 'title');
	endif;

	if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != '') ):
	$rating = wp_filter_nohtml_kses($_POST['rating']);
	update_comment_meta( $comment_id, 'rating', $rating );
	else :
	delete_comment_meta( $comment_id, 'rating');
	endif;
    
  // tampoco he visto cambio en la BD. Esto debe ser anyadir al comentario y que se guarde
  
  if ( ( isset( $_POST['pantalla'] ) ) && ( $_POST['pantalla'] != '') ):
	$pantalla = wp_filter_nohtml_kses($_POST['pantalla']);
	update_comment_meta( $comment_id, 'pantalla', $pantalla );
	else :
	delete_comment_meta( $comment_id, 'pantalla');
	endif;
	
    if ( ( isset( $_POST['sonido'] ) ) && ( $_POST['sonido'] != '') ):
	$sonido = wp_filter_nohtml_kses($_POST['sonido']);
	update_comment_meta( $comment_id, 'sonido', $sonido );
	else :
	delete_comment_meta( $comment_id, 'sonido');
	endif;
	
}

// Add the comment meta (saved earlier) to the comment text 
// You can also output the comment meta values directly in comments template  

add_filter( 'comment_text', 'modify_comment');
function modify_comment( $text ){

	$plugin_url_path = WP_PLUGIN_URL;

	if( $commenttitle = get_comment_meta( get_comment_ID(), 'title', true ) ) {
		$commenttitle = '<strong>' . esc_attr( $commenttitle ) . '</strong><br/>';
		$text = $commenttitle . $text;
	} 
	// aqui debajo tiene que estar el error.
  //
  //
  //
  //print_r( get_comment_meta( get_comment_ID(), 'rating', true ) );
 
	//if( $commentrating = get_comment_meta( get_comment_ID(), 'rating', true ) && $commentratingpa = get_comment_meta( get_comment_ID(), 'pantalla', true ) ) {
		$test = get_comment_meta( get_comment_ID(), 'rating', true );
 		$pantalla = get_comment_meta( get_comment_ID(), 'pantalla', true );
  	$sonido = get_comment_meta( get_comment_ID(), 'sonido', true );
  /* $commentrating = '<p class="comment-rating">	<img src="'. $plugin_url_path .'/ExtendComment/images/'. $commentrating . 'star.gif"/><br/>Rating: <strong>'. 

      $commentrating .' / 5</strong></p>';*/
   		
    $commentrating .= '<p class="comment-rating">	<img src="'. $plugin_url_path .'/ExtendComment/images/'. $test . 'star.gif"/><br/>Pantalla: <strong>'. $test .' / 5</strong></p>';
  $commentrating .= '<p class="comment-rating">	<img src="'. $plugin_url_path .'/ExtendComment/images/'. $pantalla . 'star.gif"/><br/>Bateria: <strong>'. $pantalla .' / 5</strong></p>';  
    $commentrating .= '<p class="comment-rating">	<img src="'. $plugin_url_path .'/ExtendComment/images/'. $sonido . 'star.gif"/><br/>Sonido: <strong>'. $sonido .' / 5</strong></p>';  
    $text = $text . $commentrating;
   
    
  
    /*print_r("puntuacion: ". get_comment_meta( get_comment_ID(), 'rating', true ) );
     print_r('path: <img src="'. $plugin_url_path .'/ExtendComment/images/'. $test . 'star.gif"/>');
  
    print_r("--------------------------------------");
  */
		return $text;		
		
    
}